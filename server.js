'use strict';

const express = require('express');
const useragent = require('useragent');

const PORT = 8080;
const HOST = '0.0.0.0';

const app = express();

app.get('/', (req, res) => {
	res.send('Hello world');
});

app.get('/agent/:user_agent', (req, res) => {
    var user_agent = req.params.user_agent;
	var agent = useragent.parse(user_agent);

	res.send(`{ value: '${agent.device.family}'}`);
});

app.listen(PORT,HOST);

console.log(`Running on http://${HOST}:${PORT}`);