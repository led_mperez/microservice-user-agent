This repo creates a microservice for a nodejs API.

To build the image:
docker build -t <your username>/useragent-microservice .

To start a container using the image:
docker run -p <host_port>:8080 -d <your username>/useragent-microservice



Requirements:

nodejs version >= 8
npm, version >= 5.6

To manually start the microservice:
1 - Instance
	a - EC2 instance from 'My AMIs' section called: Useragent Microserver Ec2.
	b - Any other host with nodejs and npm .
2 - SSH into the instance.
3 - Build the app:
	cd /usr/src/app
	npm install

3 - Start the application:
	npm start
	for a 'daemon-like' way: 
	nohup node server.js &

	


